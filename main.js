const electron = require('electron');
const app = electron.app;  // Модуль контролирующей жизненый цикл нашего приложения.
const BrowserWindow = electron.BrowserWindow;  // Модуль создающий браузерное окно.
const Menu = require("menu");
// Сохраняем глобальную ссылку на объект Window, если этого не сделать
// окно закроется автоматически как только сработает сборщик мусора JS.
let mainWindow = null;
let aboutWindow = null;

// Выйти, после того как все окна будут закрыты.
app.on('window-all-closed', function () {
    // В OS X это характерно для приложений и их меню,
    // чтобы оставаться активными, пока пользователь явно не завершит работу
    // при помощи Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});

// Этот метод будет вызван когда Electron закончил
// инициализацию и готов к созданию окна браузера.
app.on('ready', function () {
    // Создаем окно браузера.
    mainWindow = new BrowserWindow({width: 1250, height: 685});
    let template = [{
        label: "Меню",
        submenu: [
            { label: "О программе", selector: "orderFrontStandardAboutPanel:",
                click: function() {
                    if (!aboutWindow) {
                        aboutWindow = new BrowserWindow({
                            parent: mainWindow,
                            modal: true,
                            width: 590,
                            height: 430,
                            alwaysOnTop: true,
                            maximizable: false,
                            minimizable: false
                        });
                        aboutWindow.setMenu(null);
                        aboutWindow.setResizable(false);
                        aboutWindow.loadURL('file://' + __dirname + '/about.html');
                        aboutWindow.on('closed', () => aboutWindow = null);
                    }
                    aboutWindow.on('ready-to-show', () => aboutWindow.show());
                }
            },
            { type: "separator" },
            { label: "Выход", accelerator: "Command+Q", click: function() { app.quit(); }}
        ]}];
    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
    // и загружаем index.html в приложение.
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    // mainWindow.webContents.openDevTools();
    // Генерируется когда окно закрыто.
    mainWindow.on('closed', function () {
        // Сброс объекта окна, обычно нужен когда вы храните окна
        // в массиве, это нужно если в вашем приложении множество окон,
        // в таком случае вы должны удалить соответствующий элемент
        mainWindow = null;
    });
});
/**************************************************************************************/

