const perfy = require('perfy');
matrix = [];
const MAX_TIME = 100;
const MIN_TIME = 1;
let emp_number = 0, work_number = 0;
function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

generateMatrix = function () {
    clear_results("body-table-results");
    emp_number  = parseInt(document.getElementById('emp-number').value);
    work_number = parseInt(document.getElementById('work-number').value);
    let step_number = parseInt(document.getElementById('step-number').value);
    if(!validNumbers(emp_number, work_number, step_number)) return;
    let tr, td, th, input;
    let table = document.getElementById('table_time');
    table.innerHTML = "";
    //Добавляем заголовок
    tr = document.createElement("tr");
    let thead = document.createElement("thead"),
        tbody = document.createElement("tbody");
    thead.className = "thead-inverse";
    for (let j = -1; j < emp_number+1; j++){
        th = document.createElement("th");
        if(j == -1) th.innerHTML = "";
        else if(j != 0) th.innerHTML = j;
        else {
            th.innerHTML = "Этап";
            th.style.backgroundColor = "#e8e8e8";
        }
        tr.appendChild(th);
    }
    thead.appendChild(tr);
    table.appendChild(thead);
    let disrib = distributionSteps(work_number, step_number);
    //Добавляем содержимое
    for (let i = 0; i < work_number; i++){
        matrix[i] = [];
        tr = document.createElement("tr");
        th = document.createElement("th");
        th.innerHTML = i+1;
        tr.appendChild(th);
        //Добавляем ячейку с этапом
        td = document.createElement("td");
        td.style.backgroundColor = "#e8e8e8";
        input = document.createElement("input");
        input.setAttribute("type", "number");
        input.setAttribute("min", "1");
        input.className = "form-control";
        input.setAttribute("id", "step_"+i);
        input.value = disrib[i];
        td.appendChild(input);
        tr.appendChild(td);
        for (let j = 0; j < emp_number; j++){
            matrix[i][j] = getRandomInt(MIN_TIME, MAX_TIME);
            td = document.createElement("td");
            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "0");
            input.className = "form-control";
            input.setAttribute("id", i+"_"+j);
            input.value = matrix[i][j];
            // td.innerHTML = matrix[i][j];
            td.appendChild(input);
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    console.log(matrix);
};

function getMatrixValues() {
    emp_number  = parseInt(document.getElementById('emp-number').value);
    work_number = parseInt(document.getElementById('work-number').value);
    let step_number = parseInt(document.getElementById('step-number').value);
    if(!validNumbers(emp_number, work_number, step_number)) return;
    let value;
    for (let i = 0; i < work_number; i++) {
        matrix[i] = [];
        for (let j = 0; j < emp_number; j++){
            value = document.getElementById(i + "_" +j).value;
            if(value.length > 0)
                matrix[i][j] = +value;
            else {
                matrix = [];
                onError("Внимание!", "Заполните все ячейки матрицы трудозатрат!");
                return;
            }
        }
    }
    console.log(matrix);
}

function getStepsValues(work_number, step_number) {
    let distrib = [], value;
    for (let i = 0; i < work_number; i++){
        value = parseInt(document.getElementById('step_'+i).value);
        if(!isNaN(value)) distrib[i] = value;
        else{
            matrix = [];
            onError("Внимание!", "Заполните все ячейки значений этапов!");
            return;
        }
    }
    for (let i = 0; i < step_number; i++){
        if(distrib.indexOf(i+1) == -1){
            matrix = [];
            onError("Внимание!", "Пропущен этап технологической последовательности! Указано "+step_number+" этапов");
            return;
        }
    }
    return distrib;
}

resMap = new Map();
let id_row = 0;

function addSolveRow(solve, step_number) {
    if(solve){
        id_row++;
        //slice - возвращает поверхностную копию части массива в новый объект массива
        //0 - как начало "отрезания", .slice(0) быстрее, чем .slice()
        resMap.set(id_row, solve.nodes.slice(0));
        let table = document.getElementById("body-table-results");
        table.appendChild(
            (function(){
                    let td_num = document.createElement("td");
                    td_num.innerHTML = id_row;

                    let td_time = document.createElement("td");
                    td_time.innerHTML =
                        "<div>"+(solve.time.minutes<10 ? "0" + solve.time.minutes : solve.time.minutes) + ":" +
                        (solve.time.seconds < 10 ? "0" + solve.time.seconds.toFixed(3) : solve.time.seconds.toFixed(3))+ "</div>";

                    let td_cost = document.createElement("td");
                    let times = "";
                    solve.times.forEach(function(element, index, array) {times += "Этап " + index + ": " + element + "<br/>";});
                    td_cost.innerHTML = times + "Всего: " + solve.cost.toFixed(3);

                    let td_del = document.createElement("td");
                    let fun = "remove_row(this.parentElement.parentElement);";
                    let fun2 = "load_result("+id_row+");";
                    td_del.innerHTML = "<button type=\"button\" class=\"btn btn-default btn-xs\" onclick=\""+fun2+"\" title='Показать результат'>" +
                        "<span class=\"glyphicon glyphicon-eye-open\"></span>" +
                        "</button>"+
                        "<br/><button type=\"button\" class=\"btn btn-default btn-xs\" onclick=\""+fun+"\" title='Удалить строку'>" +
                        "<span class=\"glyphicon glyphicon-remove\"></span>" +
                        "</button>";
                    let tr = document.createElement("tr");
                    tr.setAttribute("id", id_row);
                    tr.appendChild(td_num);
                    tr.appendChild(td_time);
                    tr.appendChild(td_cost);
                    tr.appendChild(td_del);
                    return tr;
                }
            )()
        );
    }
}

genetic = function () {
    let iter_number  = document.getElementById('iter_number').value;
    let prob_number  = document.getElementById('prob_number').value;
    let psize_number = document.getElementById('psize_number').value;
    let esize_number = document.getElementById('esize_number').value;
    let step_number = parseInt(document.getElementById('step-number').value);
    emp_number  = parseInt(document.getElementById('emp-number').value);
    work_number = parseInt(document.getElementById('work-number').value);

    if(!validNumbers(emp_number, work_number, step_number)) return;
    if(document.getElementById("table_time").childElementCount == 0)
    {
        onError("Внимание!", "Заполните матрицу трудозатрат!");
        return;
    }
    if(matrix.length == 0) getMatrixValues();
    let distrib = getStepsValues(work_number, step_number);
    if(matrix.length == 0) return;
    perfy.start("solveGenetic");
    let result = solveGenetic.apply(null, [matrix, distrib, iter_number, prob_number, psize_number, esize_number, emp_number, work_number]);
    let time = perfy.end("solveGenetic");
    result.time = {
        minutes: Math.floor(time.seconds / 60),
        seconds: time.seconds % 60 + time.milliseconds / 1000
    };
    perfy.destroy("solveGenetic");
    console.log(result);
    addSolveRow(result, step_number);
    paintWhite(work_number, emp_number);
    screenResult(result.nodes, emp_number);
};

function solveGenetic(matrix, distrib, iterations_limit, mutation_prob, population_size, elite_size, emp_number, work_number) {
    const chromosomeLength = emp_number*work_number;
    let fitness = function(nodes) {
            let time = 0, step, rb, max = 0;
            let times = [], steps = [];
            for (let i = 0; i < chromosomeLength; i++){
                if(nodes[i] != -1){
                    if(i < emp_number) rb = i;
                    else rb = i%emp_number;
                    //nodes[i] - работа, i - рабочий
                    time = matrix[nodes[i]][rb];
                    step = distrib[nodes[i]];
                    if(steps[step] != undefined){//Уже встречался этот этап
                        if(steps[step][rb] != undefined){
                            steps[step][rb] = steps[step][rb] + time;
                        }else steps[step][rb] = time;
                    }else{
                        steps[step] = [];
                        steps[step][rb] = time;
                    }
                    if(times[step] != undefined)
                    {
                        if(times[step] < steps[step][rb]) times[step] = steps[step][rb];
                    }
                    else times[step] = steps[step][rb];
                }
            }
            times.forEach(function(element, index, array) {max += element;});
            return max;
        };
        let times = function(nodes) {
            let time = 0, step, rb, max = 0;
            let times = [], steps = [];
            for (let i = 0; i < chromosomeLength; i++){
                if(nodes[i] != -1){
                    if(i < emp_number) rb = i;
                    else rb = i%emp_number;
                    //nodes[i] - работа, i - рабочий
                    time = matrix[nodes[i]][rb];
                    step = distrib[nodes[i]];
                    if(steps[step] != undefined){//Уже встречался этот этап
                        if(steps[step][rb] != undefined){
                            steps[step][rb] = steps[step][rb] + time;
                        }else steps[step][rb] = time;
                    }else{
                        steps[step] = [];
                        steps[step][rb] = time;
                    }
                    if(times[step] != undefined)
                    {
                        if(times[step] < steps[step][rb]) times[step] = steps[step][rb];
                    }
                    else times[step] = steps[step][rb];
                }
            }
            return times;
        };
    // Помогатели
    let cmp = (l, r) => l[0] - r[0]; // Компаратор
    let swap = (arr, id1, id2) => { let t = arr[id1]; arr[id1] = arr[id2]; arr[id2] = t; }; // Обмен элементов
    // Генерируем начальную популяцию
    let population = [];
    {
        let metaChromosome = new Array(chromosomeLength);
        for (let i = 0; i < chromosomeLength; ++i){
            if(i < work_number)
                metaChromosome[i] = [null, i];
            else metaChromosome[i] = [null, -1];
        }
        for (let population_number = 0; population_number < population_size; ++population_number) {
            for (let i = 0; i < chromosomeLength; ++i)
                metaChromosome[i][0] = Math.random();
            metaChromosome.sort(cmp);
            let newChromosome = metaChromosome.reduce((chromo, gene) => { chromo.push(gene[1]); return chromo }, []);
            population.push([fitness(newChromosome), newChromosome]);
        }
    }
    population.sort(cmp);
    // Основной цикл
    for (let iteration_number = 1; iteration_number <= iterations_limit; ++iteration_number) {
        // Переносим элиту в новую популяцию
        let newPopulation = population.slice(0, elite_size);
        // Секция "Скрещивание" (по "принципу жадности")
        for (let i = 0; i < Math.ceil((population_size-elite_size)/2); ++i) {
            let cross = function (p1, p2, used) {
                used.fill(false);
                let chosen = p1[0];
                let offspring = [chosen], mp1, mp2;
                used[chosen] = true;
                for (let i = 1; i < chromosomeLength; ++i) {
                    if (!used[p1[i]] && !used[p2[i]] && p1[i] >= 0 && p2[i] >= 0) {
                        mp1 = p1[i] < 0 || offspring[i - 1] < 0 ? -1 : matrix[offspring[i - 1]][p1[i]];
                        mp2 = p2[i] < 0 || offspring[i - 1] < 0 ? -1 : matrix[offspring[i - 1]][p2[i]];
                        chosen = mp1 < mp2 ? p1[i] : p2[i];
                    }
                    else if (!used[p1[i]]) chosen = p1[i];
                    else if (!used[p2[i]]) chosen = p2[i];
                    else if (p1[i] < 0 && p2[i] < 0) chosen = -1;
                    else {
                        if (used[p1[i]] && used[p2[i]]) {
                            let not_used = [];
                            for(let j = 0; j < used.length; j ++){
                                if(!used[j]) not_used.push(j);
                            }
                            if(not_used.length != 0)
                                chosen = not_used[Math.floor(Math.random() * not_used.length)];
                            else chosen = -1;
                        }
                        else chosen = -1;
                    }
                    offspring.push(chosen);
                    used[chosen] = true;
                }
                return offspring;
            };
            // Выбираем родителей
            let parent1 = Math.floor(Math.random() * population_size),
                parent2 = parent1 + Math.floor(Math.random() * (population_size - parent1));
            parent1 = population[parent1][1].slice();
            parent2 = population[parent2][1].slice();
            // Даём потомство
            let used = new Array(work_number);
            newPopulation.push(
                [0, cross(parent1, parent2, used)],
                [0, cross(parent2, parent1, used)]
            );
        }
        // Секция "Мутация" (случ. часть хромосомы инвертируется)
        for (let i = elite_size; i < newPopulation.length; ++i)
            if (mutation_prob > Math.random()) {
                let left = Math.floor(Math.random() * (chromosomeLength - 1)),
                    right = Math.round(left + 1 + Math.random() * chromosomeLength / 2);
                if (right > chromosomeLength - 1) right = chromosomeLength - 1;
                for (let j = 0; j < (right-left)/2; ++j)
                    swap(newPopulation[i][1], left + j, right - j);
            }
        // Вычисляем выживаемость
        for (let i = elite_size; i < newPopulation.length; ++i)
            newPopulation[i][0] = fitness(newPopulation[i][1]);

        // Сортируем по выживаемости и убиваем слабых. Это СПАРТА!
        newPopulation.sort(cmp);
        if (newPopulation.length > population_size)
            newPopulation.splice(population_size - newPopulation.length);
        population = newPopulation;
    }
    // Выбираем Выжившего
    return {
        nodes: population[0][1],
        cost: population[0][0],
        times: times(population[0][1])
    };
}